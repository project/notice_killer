<?php

/**
 * Implements hook_token_info().
 */
function notice_killer_token_info() {
  $info = array();

  $info['types']['notice_killer_log_entry'] = array(
    'name' => t('Notice killer log entry'),
    'description' => t('Log entries created by the notice killer module'),
    'needs-data' => 'notice_killer_log_entry',
  );

  //hook_token_info_item_types
  $info['tokens']['notice_killer_log_entry']['type'] = array(
    'name' => t('Type'),
    'description' => t('The type of log message'),
  );

  $info['tokens']['notice_killer_log_entry']['message'] = array(
    'name' => t('Message'),
    'description' => t('The log message'),
  );

  $info['tokens']['notice_killer_log_entry']['severity'] = array(
    'name' => t('Severity'),
    'description' => t('Severity'),
  );

  $info['tokens']['notice_killer_log_entry']['request-uri'] = array(
    'name' => t('Request URI'),
    'description' => t('Request URI where the log message occurred.'),
  );

  $info['tokens']['notice_killer_log_entry']['view-url'] = array(
    'name' => t('View URL'),
    'description' => t('The URL to view more information about this event.'),
  );

  $info['tokens']['notice_killer_log_entry']['logged-time'] = array(
    'name' => t('Logged time'),
    'description' => t('The time and date the event was logged.'),
    'type' => 'date',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function notice_killer_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  if ($type == 'notice_killer_log_entry' && !empty($data['notice_killer_log_entry'])) {
    $log_entry = $data['notice_killer_log_entry'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'type':
          $replacements[$original] = $sanitize ? check_plain($log_entry['type']) : $log_entry['type'];
          break;

        case 'message':
          $replacements[$original] = $sanitize ? check_plain(format_string($log_entry['message'], $log_entry['variables'])) : format_string($log_entry['message'], $log_entry['variables']);
          break;

        case 'severity':
          $severity = watchdog_severity_levels();
          $replacements[$original] = $sanitize ? check_plain($severity[$log_entry['severity']]) : $severity[$log_entry['severity']];
          break;

        case 'request-uri':
          $replacements[$original] = $sanitize ? check_plain($log_entry['request_uri']) : $log_entry['request_uri'];
          break;

        case 'view-url':
          $replacements[$original] = url('admin/reports/notices/event/' . $log_entry['wid'], $url_options);
          break;

        case 'logged-time':
          $replacements[$original] = format_date($log_entry['timestamp'], 'medium', '', NULL, $language_code);
          break;
      }
    }

    if ($created_tokens = token_find_with_prefix($tokens, 'logged-time')) {
      $replacements += token_generate('date', $created_tokens, array('date' => $log_entry['timestamp']), $options);
    }
  }
  return $replacements;
}
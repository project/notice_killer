<?php

/**
 * Page callback: Displays a listing of database log messages.
 *
 * Messages are truncated at 56 chars. Full-length messages can be viewed on the
 * message details page.
 *
 * @see dblog_clear_log_form()
 * @see dblog_event()
 * @see dblog_filter_form()
 * @see dblog_menu()
 *
 * @ingroup logging_severity_levels
 */
function notice_killer_overview() {
  $rows = array();
  $classes = array(
    WATCHDOG_DEBUG     => 'dblog-debug',
    WATCHDOG_INFO      => 'dblog-info',
    WATCHDOG_NOTICE    => 'dblog-notice',
    WATCHDOG_WARNING   => 'dblog-warning',
    WATCHDOG_ERROR     => 'dblog-error',
    WATCHDOG_CRITICAL  => 'dblog-critical',
    WATCHDOG_ALERT     => 'dblog-alert',
    WATCHDOG_EMERGENCY => 'dblog-emerg',
  );

  $build['dblog_clear_log_form'] = drupal_get_form('notice_killer_clear_log_form');

  $header = array(
    '', // Icon column.
    array('data' => t('Last logged'), 'field' => 'w.last_timestamp', 'sort' => 'desc'),
    array('data' => t('First logged'), 'field' => 'w.wid', 'sort' => 'desc'),
    array('data' => t('Appearances'), 'field' => 'w.count', 'sort' => 'desc'),
    t('Message'),
    array('data' => t('User'), 'field' => 'u.name'),
  );

  $query = db_select('notice_killer', 'w')->extend('PagerDefault')->extend('TableSort');
  $query->leftJoin('users', 'u', 'w.uid = u.uid');
  $query
    ->fields('w', array('wid', 'uid', 'severity', 'timestamp', 'message', 'variables', 'last_timestamp', 'count', 'type'))
    ->addField('u', 'name');
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  foreach ($result as $dblog) {
    $rows[] = array('data' =>
      array(
        // Cells
        array('class' => 'icon'),
        format_date($dblog->last_timestamp, 'short'),
        format_date($dblog->timestamp, 'short'),
        $dblog->count,
        theme('notice_killer_message', array('event' => $dblog, 'link' => TRUE)),
        theme('username', array('account' => $dblog)),
      ),
      // Attributes for tr
      'class' => array(drupal_html_class('dblog-' . $dblog->type), $classes[$dblog->severity]),
    );
  }

  $build['dblog_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'admin-dblog'),
    '#empty' => t('No log messages available.'),
  );
  $build['dblog_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Returns HTML for a log message.
 *
 * @param array $variables
 *   An associative array containing:
 *   - event: An object with at least the message and variables properties.
 *   - link: (optional) Format message as link, event->wid is required.
 *
 * @ingroup themeable
 */
function theme_notice_killer_message($variables) {
  $output = '';
  $event = $variables['event'];
  // Check for required properties.
  if (isset($event->message) && isset($event->variables)) {
    // Messages without variables or user specified text.
    if ($event->variables === 'N;') {
      $output = $event->message;
    }
    // Message to translate with injected variables.
    else {
      $output = t($event->message, unserialize($event->variables));
    }
    if ($variables['link'] && isset($event->wid)) {
      // Truncate message to 80 chars.
      $output = truncate_utf8(filter_xss($output, array()), 80, TRUE, TRUE);
      $output = l($output, 'admin/reports/notices/event/' . $event->wid, array('html' => TRUE));
    }
  }
  return $output;
}

/**
 * Page callback: Displays details about a specific database log message.
 *
 * @param int $id
 *   Unique ID of the database log message.
 *
 * @return array|string
 *   If the ID is located in the Database Logging table, a build array in the
 *   format expected by drupal_render(); otherwise, an empty string.
 *
 * @see dblog_menu()
 */
function notice_killer_event($id) {
  $severity = watchdog_severity_levels();
  $result = db_query('SELECT w.*, u.name, u.uid FROM {notice_killer} w INNER JOIN {users} u ON w.uid = u.uid WHERE w.wid = :id', array(':id' => $id))->fetchObject();
  if ($dblog = $result) {
    $rows = array(
      array(
        array('data' => t('Type'), 'header' => TRUE),
        t($dblog->type),
      ),
      array(
        array('data' => t('First appearance date'), 'header' => TRUE),
        format_date($dblog->timestamp, 'long'),
      ),
      array(
        array('data' => t('Last appearance date'), 'header' => TRUE),
        format_date($dblog->last_timestamp, 'long'),
      ),
      array(
        array('data' => t('Appearances'), 'header' => TRUE),
        $dblog->count,
      ),
      array(
        array('data' => t('User'), 'header' => TRUE),
        theme('username', array('account' => $dblog)),
      ),
      array(
        array('data' => t('Location'), 'header' => TRUE),
        l($dblog->location, $dblog->location),
      ),
      array(
        array('data' => t('Referrer'), 'header' => TRUE),
        l($dblog->referer, $dblog->referer),
      ),
      array(
        array('data' => t('Message'), 'header' => TRUE),
        theme('notice_killer_message', array('event' => $dblog)),
      ),
      array(
        array('data' => t('Severity'), 'header' => TRUE),
        $severity[$dblog->severity],
      ),
      array(
        array('data' => t('Hostname'), 'header' => TRUE),
        check_plain($dblog->hostname),
      ),
//      array(
//        array('data' => t('Operations'), 'header' => TRUE),
//        $dblog->link,
//      ),
    );
    $build['dblog_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#attributes' => array('class' => array('dblog-event')),
    );

    // Add in the backtrace if we have one.
    if (!empty($dblog->backtrace) && ($backtrace = unserialize($dblog->backtrace))) {
      // Print the stacktrace.
      $build['backtrace'] = array(
        '#type' => 'fieldset',
        '#title' => t('Backtrace'),
      );

      $backtrace_header = array(
        t('Function'),
        t('File'),
        t('Line'),

      );
      $backtrace_rows = array();
      foreach ($backtrace as $row) {
        $backtrace_rows[] = array(
          check_plain(notice_killer_backtrace_function_name($row)),
          isset($row['file']) ? check_plain($row['file']) : '',
          isset($row['line']) ? check_plain($row['line']) : '',
        );
      }

      $build['backtrace']['table'] = array(
        '#theme' => 'table',
        '#rows' => $backtrace_rows,
        '#header' => $backtrace_header,
      );

      if (function_exists('kprint_r')) {
        $build['backtrace']['kprint'] = array(
          '#markup' => kprint_r($backtrace, TRUE),
        );
      }
      else {
        $build['backtrace']['kprint'] = array(
          '#markup' => t('You will get more information about the backtrace if you enable the <a href="@devel_link">Devel module</a>.', array('@devel_link' => url('https://www.drupal.org/project/devel', array('external' => TRUE)))),
        );
      }
    }

    // Add in some actions.
    $build['actions']['delete'] = array(
      '#type' => 'link',
      '#href' => 'admin/reports/notices/event/' . $id . '/delete/' . drupal_get_token('notice_killer_' . $id),
      '#title' => t('Delete this log entry'),
      '#options' => array(
        'attributes' => array(
          'class' => array(
            'button',
          ),
        ),
      ),
    );

    return $build;
  }
  else {
    return '';
  }
}

/**
 * Return data on the function name for a specific backtrace row.
 *
 * @param $backtrace_row
 *   The row from the backtrace to print.
 *
 * @return string
 *   The computed function name.
 */
function notice_killer_backtrace_function_name($backtrace_row) {
  $function = isset($backtrace_row['function']) ? check_plain($backtrace_row['function']) : '';
  if (isset($backtrace_row['class']) && isset($backtrace_row['type'])) {
    $function = $backtrace_row['class'] . $backtrace_row['type'] . $function;
  }
  return $function;
}

/**
 * Delete notice killer event page callback.
 *
 * @param $event
 */
function notice_killer_event_delete($event) {
  db_delete('notice_killer')
    ->condition('wid', $event)
    ->execute();
  drupal_set_message(t('The event was deleted'));
  drupal_goto('admin/reports/notices');
}

/**
 * Form constructor for the form that clears out the log.
 *
 * @see notice_killer_clear_log_form_submit()
 * @ingroup forms
 */
function notice_killer_clear_log_form($form) {
  $form['dblog_clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear log messages'),
    '#description' => t('This will permanently remove the log messages from the database.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['dblog_clear']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log messages'),
    '#submit' => array('notice_killer_clear_log_form_submit'),
  );

  return $form;
}

/**
 * Form submission handler for notice_killer_clear_log_form().
 */
function notice_killer_clear_log_form_submit() {
  db_delete('notice_killer')->execute();
  drupal_set_message(t('Database log cleared.'));
}
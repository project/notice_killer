<?php

/**
 * Implementation of hook_drush_sql_sync_sanitize().
 */
function notice_killer_drush_sql_sync_sanitize() {
  // Fetch list of all tables.
  $all_tables = drush_sql_get_class()->listTables();

  $queries = array();

  if (in_array('notice_killer', $all_tables, TRUE)) {
    $queries[] = "DELETE FROM notice_killer";
  }
  if (in_array('watchdog', $all_tables, TRUE)) {
    $queries[] = "DELETE FROM watchdog";
  }

  if (!empty($queries)) {
    $queries[] = '';
    drush_sql_register_post_sync_op('notice_killer', dt('Clear Notice Killer/Watchdog logs.'), implode(';', $queries));
  }
}
